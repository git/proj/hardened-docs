#!/bin/sh

if [ $# -ne 0 ];
then
  echo "Usage: $0";
  echo "";
  echo "This script will generate and fix the HTML files inside this directory.";
  echo "For this having gorg installed is necessary.";
  exit 1;
fi

if [ -z "$(whereis -b gorg | cut -d: -f2)" ];
 then
  echo "gorg not found, can't continue without it :(";
  exit 1;
fi

rm -r html/
for FILE in `find xml/ -type f`;
do
  output=html/${FILE#xml/}
  mkdir -p `dirname $output`
  case $FILE in
    *~)
      rm $FILE
      ;;
    *.xml)
      output=${output%.xml}.html
      gorg < $FILE | \
           sed -e 's|"/css/main.css"|"http://www.gentoo.org/css/main.css"|g' \
               -e 's|"../../../|"http://www.gentoo.org/|g' \
               -e 's|"/images/|"http://www.gentoo.org/images/|g' \
	       -e 's|"images/|"http://www.gentoo.org/images/|g' \
               -e 's|"/|"http://www.gentoo.org/|g' \
               -e 's|"http://www.gentoo.org/proj/en/hardened/\([^"]*\).xml"|"\1.html"|g' \
	       -e 's|"css/main.css"|"http://www.gentoo.org/css/main.css"|g' \
	       -e 's|"../../css/|"http://www.gentoo.org/css/|g' \
	       -e 's|"../../images/|"http://www.gentoo.org/images/|g' \
               -e 's|"http://www.gentoo.org/proj/en/hardened/\([^"]*\)"|"\1"|g' | \
           tr -d "\302" | tr -d "\240" > $output;
      ;;
    *)
      cp $FILE $output
      ;;
  esac
done
