Rationale
=========
Although the use of specfiles as done ino on patches2 gcc 4.3 was a very nice solution it had
a big maintainability problem as the flags weren't updated for other rules in
the same run to use. This meant that we needed a lot of rules in order to add
the hardened compiler behaviour, which in turn meant more complex
maintainability.

What the new solution does is patching gcc so it runs the hardened specfiles
before the mainfile so the flag modifications are visible to it. But, as this is
not needed by everybody, this is only done when the configure script gets the
argument --enable-esp.

Modifications
=============

[04:05] <Zorry> if you look in the gcc.c patch
[04:07] <Zorry> fist we inlude header file that include moste of the specs
[04:09] <Zorry> hunk 2 we use a diffrent CC1_SPEC
[04:10] <Zorry> hunk 3-5 add spec rules for ssp rules that is in esp.h
[04:11] <Zorry> hunk 6 add ESP_EXTRA_SPECS that is defined in esp.h
[04:12] <Zorry> hunk 9 add the needed hardened spec rule so it rune befor all the rest of the spec rules

20_all... Adds defines and constants an


now we do in esp.h -> 30_...


Previously, lots of patches to modify specs
Now just 10 lines (or so)


Now we modify to include the new header and add options before calling (on gcc.c 20...)


ebuild indicates stable featureset.

Older ones have no SSP support (stable)



eclass ~146 sets ssp and pie USEs
Info on patches ~239

~354 get sources for patches...

~405 ssp pie checks.

~514 we want SSP/PIE?

~612 set CFLAGS and branding

~1160 enable stuff per version

~2096 unpack stuff

~2286 add things to makefile and pie version

-----------

10 add options to configure including linker opts certain defines...
crtbeginTS support...

11some defines

12Makefile add nossp and nopie supports (-fno-stack-protector) Code to test crtbeginTS... add files used later... and fixes

40 patch for specs

24 use special flags to solve static problems.