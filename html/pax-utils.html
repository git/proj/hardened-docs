<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link title="new" rel="stylesheet" href="http://www.gentoo.org/css/main.css" type="text/css">
<link REL="shortcut icon" HREF="http://www.gentoo.org/favicon.ico" TYPE="image/x-icon">
<link rel="search" type="application/opensearchdescription+xml" href="http://www.gentoo.org/search/www-gentoo-org.xml" title="Gentoo Website">
<link rel="search" type="application/opensearchdescription+xml" href="http://www.gentoo.org/search/forums-gentoo-org.xml" title="Gentoo Forums">
<link rel="search" type="application/opensearchdescription+xml" href="http://www.gentoo.org/search/bugs-gentoo-org.xml" title="Gentoo Bugzilla">
<link rel="search" type="application/opensearchdescription+xml" href="http://www.gentoo.org/search/packages-gentoo-org.xml" title="Gentoo Packages">
<link rel="search" type="application/opensearchdescription+xml" href="http://www.gentoo.org/search/archives-gentoo-org.xml" title="Gentoo List Archives">
<title>Gentoo Linux Documentation
--
  Gentoo PaX Utilities</title>
</head>
<body style="margin:0px;" bgcolor="#ffffff"><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td valign="top" height="125" bgcolor="#45347b"><a href="http://www.gentoo.org/"><img border="0" src="http://www.gentoo.org/images/gtop-www.jpg" alt="Gentoo Logo"></a></td></tr>
<tr><td valign="top" align="right" colspan="1" bgcolor="#ffffff"><table border="0" cellspacing="0" cellpadding="0" width="100%"><tr>
<td width="99%" class="content" valign="top" align="left">
<br><h1>Gentoo PaX Utilities</h1>
<form name="contents" action="http://www.gentoo.org">
<b>Content</b>:
        <select name="url" size="1" OnChange="location.href=form.url.options[form.url.selectedIndex].value" style="font-family:sans-serif,Arial,Helvetica"><option value="#doc_chap1">1. What is this guide about?</option>
<option value="#doc_chap2">2. Extracting ELF Information from Binaries</option>
<option value="#doc_chap3">3. Listing PaX Flags and Capabilities</option>
<option value="#doc_chap4">4. Programming with ELF files</option></select>
</form>
<p class="chaphead"><a name="doc_chap1"></a><span class="chapnum">1.
            </span>What is this guide about?</p>
<p class="secthead"><a name="doc_chap1_sect1">Introduction</a></p>
<p>
The security of a system goes beyond setting up a decent firewall and good 
service configurations. The binaries you run, the libraries you load, might 
also be vulnerable against attacks. Although the exact vulnerabilities are not 
known until they are discovered, there are ways to prevent them from happening.
</p>
<p>
One possible attack vector is to make advantage of writable <span class="emphasis">and</span>
executable segments in a program or library, allowing malicious users to run
their own code using the vulnerable application or library.
</p>
<p>
This guide will inform you how to use the <span class="code" dir="ltr">pax-utils</span> package to find
and identify problematic binaries. We will also cover the use of <span class="code" dir="ltr">pspax</span> (a
tool to view PaX-specific capabilities) and <span class="code" dir="ltr">dumpelf</span> (a tool that prints
out a C structure containing a workable copy of a given object).
</p>
<p>
But before we start with that, some information on <span class="emphasis">objects</span> is in place.
Users familiar with segments and dynamic linking will not learn anything from
this and can immediately continue with <a href="#scanelf">Extracting ELF
Information from Binaries</a>.
</p>
<p class="secthead"><a name="doc_chap1_sect2">ELF objects</a></p>
<p>
Every executable binary on your system is structured in a specific way,
allowing the Linux kernel to load and execute the file. Actually, this goes 
beyond plain executable binaries: this also holds for shared objects; more 
about those later.
</p>
<p>
The structure of such a binary is defined in the ELF standard. ELF stands for
<span class="emphasis">Executable and Linkable Format</span>. If you are really interested in the gory
details, check out the <a href="http://refspecs.linux-foundation.org/LSB_4.0.0/LSB-Core-generic/LSB-Core-generic/elf-generic.html">
Generic ELF spec</a> or the <span class="code" dir="ltr">elf(5)</span> man page.
</p>
<p>
An executable ELF file has the following parts:
</p>
<ul>
  <li>
    The <span class="emphasis">ELF header</span> contains information on the <span class="emphasis">type</span> of file (is it
    an executable, a shared library, ...), the target architecture, the
    location of the Program Header, Section Header and String Header in the
    file and the location of the first executable instruction
  </li>
  <li>
    The <span class="emphasis">Program Header</span> informs the system how to create a process from
    the binary file. It is actually a table consisting of entries for each
    segment in the program. Each entry contains the type, addresses (physical 
    and virtual), size, access rights, ...
  </li>
  <li>
    The <span class="emphasis">Section Header</span> is a table consisting of entries for each section
    in the program. Each entry contains the name, type, size, ... and
    <span class="emphasis">what</span> information the section holds.
  </li>
  <li>
    Data, containing the sections and segments mentioned previously.
  </li>
</ul>
<p>
A <span class="emphasis">section</span> is a small unit consisting of specific data: instructions,
variable data, symbol table, relocation information, and so on. A <span class="emphasis">segment</span>
is a collection of sections; segments are the units that are actually
transferred to memory.
</p>
<p class="secthead"><a name="doc_chap1_sect3">Shared Objects</a></p>
<p>
Way back when, every application binary contained <span class="emphasis">everything</span> it needed to
operate correctly. Such binaries are called <span class="emphasis">statically linked</span> binaries.
They are, however, space consuming since different applications use the same 
functions over and over again.
</p>
<p>
A <span class="emphasis">shared object</span> contains the definition and instructions for such
functions. Every application that wants can <span class="emphasis">dynamically</span> link against such
a shared object so that it can benefit from the already existing functionality.
</p>
<p>
An application that is dynamically linked to a shared object contains
<span class="emphasis">symbols</span>, references for the real functionality. When such an application
is loaded in memory, it will first ask the runtime linker to resolve each and
every symbol it has. The runtime linker will load the appropriate shared objects
in memory and resolve the symbolic references between them.
</p>
<p class="secthead"><a name="doc_chap1_sect4">Segments and Sections</a></p>
<p>
How the ELF file is looked upon depends on the view we have: when we are dealing
with a binary file in Execution View, the ELF file contains segments. When 
the file is seen in Linking View, the ELF file contains sections. 
One segment spans just one or more (continuous) sections.
</p>
<p class="chaphead"><a name="scanelf"></a><a name="doc_chap2"></a><span class="chapnum">2.
            </span>Extracting ELF Information from Binaries</p>
<p class="secthead"><a name="doc_chap2_sect1">The scanelf Application</a></p>
<p>
The <span class="code" dir="ltr">scanelf</span> application is part of the <span class="code" dir="ltr">app-misc/pax-utils</span> package.
With this application you can print out information specific to the ELF
structure of a binary. The following table sums up the various options.
</p>
<table class="ntable">
<tr>
  <td class="infohead"><b>Option</b></td>
  <td class="infohead"><b>Long Option</b></td>
  <td class="infohead"><b>Description</b></td>
</tr>
<tr>
  <td class="tableinfo">-p</td>
  <td class="tableinfo">--path</td>
  <td class="tableinfo">Scan all directories in PATH environment</td>
</tr>
<tr>
  <td class="tableinfo">-l</td>
  <td class="tableinfo">--ldpath</td>
  <td class="tableinfo">Scan all directories in /etc/ld.so.conf</td>
</tr>
<tr>
  <td class="tableinfo">-R</td>
  <td class="tableinfo">--recursive</td>
  <td class="tableinfo">Scan directories recursively</td>
</tr>
<tr>
  <td class="tableinfo">-m</td>
  <td class="tableinfo">--mount</td>
  <td class="tableinfo">Don't recursively cross mount points</td>
</tr>
<tr>
  <td class="tableinfo">-y</td>
  <td class="tableinfo">--symlink</td>
  <td class="tableinfo">Don't scan symlinks</td>
</tr>
<tr>
  <td class="tableinfo">-A</td>
  <td class="tableinfo">--archives</td>
  <td class="tableinfo">Scan archives (.a files)</td>
</tr>
<tr>
  <td class="tableinfo">-L</td>
  <td class="tableinfo">--ldcache</td>
  <td class="tableinfo">Utilize ld.so.cache information (use with -r/-n)</td>
</tr>
<tr>
  <td class="tableinfo">-X</td>
  <td class="tableinfo">--fix</td>
  <td class="tableinfo">Try and 'fix' bad things (use with -r/-e)</td>
</tr>
<tr>
  <td class="tableinfo">-z [arg]</td>
  <td class="tableinfo">--setpax [arg]</td>
  <td class="tableinfo">Sets EI_PAX/PT_PAX_FLAGS to [arg] (use with -Xx)</td>
</tr>
<tr>
  <td class="infohead"><b>Option</b></td>
  <td class="infohead"><b>Long Option</b></td>
  <td class="infohead"><b>Description</b></td>
</tr>
<tr>
  <td class="tableinfo">-x</td>
  <td class="tableinfo">--pax</td>
  <td class="tableinfo">Print PaX markings</td>
</tr>
<tr>
  <td class="tableinfo">-e</td>
  <td class="tableinfo">--header</td>
  <td class="tableinfo">Print GNU_STACK/PT_LOAD markings</td>
</tr>
<tr>
  <td class="tableinfo">-t</td>
  <td class="tableinfo">--textrel</td>
  <td class="tableinfo">Print TEXTREL information</td>
</tr>
<tr>
  <td class="tableinfo">-r</td>
  <td class="tableinfo">--rpath</td>
  <td class="tableinfo">Print RPATH information</td>
</tr>
<tr>
  <td class="tableinfo">-n</td>
  <td class="tableinfo">--needed</td>
  <td class="tableinfo">Print NEEDED information</td>
</tr>
<tr>
  <td class="tableinfo">-i</td>
  <td class="tableinfo">--interp</td>
  <td class="tableinfo">Print INTERP information</td>
</tr>
<tr>
  <td class="tableinfo">-b</td>
  <td class="tableinfo">--bind</td>
  <td class="tableinfo">Print BIND information</td>
</tr>
<tr>
  <td class="tableinfo">-S</td>
  <td class="tableinfo">--soname</td>
  <td class="tableinfo">Print SONAME information</td>
</tr>
<tr>
  <td class="tableinfo">-s [arg]</td>
  <td class="tableinfo">--symbol [arg]</td>
  <td class="tableinfo">Find a specified symbol</td>
</tr>
<tr>
  <td class="tableinfo">-k [arg]</td>
  <td class="tableinfo">--section [arg]</td>
  <td class="tableinfo">Find a specified section</td>
</tr>
<tr>
  <td class="tableinfo">-N [arg]</td>
  <td class="tableinfo">--lib [arg]</td>
  <td class="tableinfo">Find a specified library</td>
</tr>
<tr>
  <td class="tableinfo">-g</td>
  <td class="tableinfo">--gmatch</td>
  <td class="tableinfo">Use strncmp to match libraries. (use with -N)</td>
</tr>
<tr>
  <td class="tableinfo">-T</td>
  <td class="tableinfo">--textrels</td>
  <td class="tableinfo">Locate cause of TEXTREL</td>
</tr>
<tr>
  <td class="tableinfo">-E [arg]</td>
  <td class="tableinfo">--etype [arg]</td>
  <td class="tableinfo">Print only ELF files matching etype ET_DYN,ET_EXEC ...</td>
</tr>
<tr>
  <td class="tableinfo">-M [arg]</td>
  <td class="tableinfo">--bits [arg]</td>
  <td class="tableinfo">Print only ELF files matching numeric bits</td>
</tr>
<tr>
  <td class="tableinfo">-a</td>
  <td class="tableinfo">--all</td>
  <td class="tableinfo">Print all scanned info (-x -e -t -r -b)</td>
</tr>
<tr>
  <td class="infohead"><b>Option</b></td>
  <td class="infohead"><b>Long Option</b></td>
  <td class="infohead"><b>Description</b></td>
</tr>
<tr>
  <td class="tableinfo">-q</td>
  <td class="tableinfo">--quiet</td>
  <td class="tableinfo">Only output 'bad' things</td>
</tr>
<tr>
  <td class="tableinfo">-v</td>
  <td class="tableinfo">--verbose</td>
  <td class="tableinfo">Be verbose (can be specified more than once)</td>
</tr>
<tr>
  <td class="tableinfo">-F [arg]</td>
  <td class="tableinfo">--format [arg]</td>
  <td class="tableinfo">Use specified format for output</td>
</tr>
<tr>
  <td class="tableinfo">-f [arg]</td>
  <td class="tableinfo">--from [arg]</td>
  <td class="tableinfo">Read input stream from a filename</td>
</tr>
<tr>
  <td class="tableinfo">-o [arg]</td>
  <td class="tableinfo">--file [arg]</td>
  <td class="tableinfo">Write output stream to a filename</td>
</tr>
<tr>
  <td class="tableinfo">-B</td>
  <td class="tableinfo">--nobanner</td>
  <td class="tableinfo">Don't display the header</td>
</tr>
<tr>
  <td class="tableinfo">-h</td>
  <td class="tableinfo">--help</td>
  <td class="tableinfo">Print this help and exit</td>
</tr>
<tr>
  <td class="tableinfo">-V</td>
  <td class="tableinfo">--version</td>
  <td class="tableinfo">Print version and exit</td>
</tr>
</table>
<p>
The format specifiers for the <span class="code" dir="ltr">-F</span> option are given in the following table.
Prefix each specifier with <span class="code" dir="ltr">%</span> (verbose) or <span class="code" dir="ltr">#</span> (silent) accordingly.
</p>
<table class="ntable">
<tr>
  <td class="infohead"><b>Specifier</b></td>
  <td class="infohead"><b>Full Name</b></td>
  <td class="infohead"><b>Specifier</b></td>
  <td class="infohead"><b>Full Name</b></td>
</tr>
<tr>
  <td class="tableinfo">F</td>
  <td class="tableinfo">Filename</td>
  <td class="tableinfo">x</td>
  <td class="tableinfo">PaX Flags</td>
</tr>
<tr>
  <td class="tableinfo">e</td>
  <td class="tableinfo">STACK/RELRO</td>
  <td class="tableinfo">t</td>
  <td class="tableinfo">TEXTREL</td>
</tr>
<tr>
  <td class="tableinfo">r</td>
  <td class="tableinfo">RPATH</td>
  <td class="tableinfo">n</td>
  <td class="tableinfo">NEEDED</td>
</tr>
<tr>
  <td class="tableinfo">i</td>
  <td class="tableinfo">INTERP</td>
  <td class="tableinfo">b</td>
  <td class="tableinfo">BIND</td>
</tr>
<tr>
  <td class="tableinfo">s</td>
  <td class="tableinfo">Symbol</td>
  <td class="tableinfo">N</td>
  <td class="tableinfo">Library</td>
</tr>
<tr>
  <td class="tableinfo">o</td>
  <td class="tableinfo">Type</td>
  <td class="tableinfo">p</td>
  <td class="tableinfo">File name</td>
</tr>
<tr>
  <td class="tableinfo">f</td>
  <td class="tableinfo">Base file name</td>
  <td class="tableinfo">k</td>
  <td class="tableinfo">Section</td>
</tr>
<tr>
  <td class="tableinfo">a</td>
  <td class="tableinfo">ARCH/e_machine</td>
  <td class="tableinfo"></td>
  <td class="tableinfo"></td>
</tr>
</table>
<p class="secthead"><a name="doc_chap2_sect2">Using scanelf for Text Relocations</a></p>
<p>
As an example, we will use <span class="code" dir="ltr">scanelf</span> to find binaries containing text
relocations.
</p>
<p>
A relocation is an operation that rewrites an address in a loaded segment. Such
an address rewrite can happen when a segment has references to a shared object
and that shared object is loaded in memory. In this case, the references are
substituted with the real address values. Similar events can occur inside the 
shared object itself.
</p>
<p>
A text relocation is a relocation in the text segment. Since text segments
contain executable code, system administrators might prefer not to have these
segments writable. This is perfectly possible, but since text relocations
actually write in the text segment, it is not always feasible. 
</p>
<p>
If you want to eliminate text relocations, you will need to make sure
that the application and shared object is built with <span class="emphasis">Position Independent
Code</span> (PIC), making references obsolete. This not only increases security,
but also increases the performance in case of shared objects (allowing writes in
the text segment requires a swap space reservation and a private copy of the
shared object for each application that uses it).
</p>
<p>
The following example will search your library paths recursively, without
leaving the mounted file system and ignoring symbolic links, for any ELF binary
containing a text relocation:
</p>
<a name="doc_chap2_pre1"></a><table class="ntable" width="100%" cellspacing="0" cellpadding="0" border="0">
<tr><td bgcolor="#7a5ada"><p class="codetitle">Code Listing2.1: Scanning the system for text relocation binaries</p></td></tr>
<tr><td bgcolor="#eeeeff" align="left" dir="ltr"><pre>
# <span class="code-input">scanelf -lqtmyR</span>
</pre></td></tr>
</table>
<p>
If you want to scan your entire system for <span class="emphasis">any</span> file containing text
relocations:
</p>
<a name="doc_chap2_pre2"></a><table class="ntable" width="100%" cellspacing="0" cellpadding="0" border="0">
<tr><td bgcolor="#7a5ada"><p class="codetitle">Code Listing2.2: Scanning the entire system for text relocation files</p></td></tr>
<tr><td bgcolor="#eeeeff" align="left" dir="ltr"><pre>
# <span class="code-input">scanelf -qtmyR /</span>
</pre></td></tr>
</table>
<p class="secthead"><a name="doc_chap2_sect3">Using scanelf for Specific Header</a></p>
<p>
The scanelf util can be used to quickly identify files that contain a 
given section header using the -k .section option.
</p>
<p>
In this example we are looking for all files in /usr/lib/debug 
recursively using a format modifier with quiet mode enabled that have been 
stripped. A stripped elf will lack a .symtab entry, so we use the '!' 
to invert the matching logic.
</p>
<a name="doc_chap2_pre3"></a><table class="ntable" width="100%" cellspacing="0" cellpadding="0" border="0">
<tr><td bgcolor="#7a5ada"><p class="codetitle">Code Listing2.3: Scanning for stripped or non stripped executables</p></td></tr>
<tr><td bgcolor="#eeeeff" align="left" dir="ltr"><pre>
# <span class="code-input">scanelf -k '!.symtab' /usr/lib/debug -Rq -F%F#k</span>
</pre></td></tr>
</table>
<p class="secthead"><a name="doc_chap2_sect4">Using scanelf for Specific Segment Markings</a></p>
<p>
Each segment has specific flags assigned to it in the Program Header of the
binary. One of those flags is the type of the segment. Interesting values are
PT_LOAD (the segment must be loaded in memory from file), PT_DYNAMIC (the
segment contains dynamic linking information), PT_INTERP (the segment 
contains the name of the program interpreter), PT_GNU_STACK (a GNU extension
for the ELF format, used by some stack protection mechanisms), and PT_PAX_FLAGS
(a PaX extension for the ELF format, used by the security-minded 
<a href="http://pax.grsecurity.net/">PaX Project</a>.
</p>
<p>
If we want to scan all executables in the current working directory, PATH
environment and library paths and report those who have a writable and
executable PT_LOAD or PT_GNU_STACK marking, you could use the following command:
</p>
<a name="doc_chap2_pre4"></a><table class="ntable" width="100%" cellspacing="0" cellpadding="0" border="0">
<tr><td bgcolor="#7a5ada"><p class="codetitle">Code Listing2.4: Scanning for Write/eXecute flags for PT_LOAD and PT_GNU_STACK</p></td></tr>
<tr><td bgcolor="#eeeeff" align="left" dir="ltr"><pre>
# <span class="code-input">scanelf -lpqe .</span>
</pre></td></tr>
</table>
<p class="secthead"><a name="doc_chap2_sect5">Using scanelf's Format Modifier Handler</a></p>
<p>
A useful feature of the <span class="code" dir="ltr">scanelf</span> utility is the format modifier handler.  
With this option you can control the output of <span class="code" dir="ltr">scanelf</span>, thereby 
simplifying parsing the output with scripts.
</p>
<p>
As an example, we will use <span class="code" dir="ltr">scanelf</span> to print the file names that contain
text relocations:
</p>
<a name="doc_chap2_pre5"></a><table class="ntable" width="100%" cellspacing="0" cellpadding="0" border="0">
<tr><td bgcolor="#7a5ada"><p class="codetitle">Code Listing2.5: Example of the scanelf format modifier handler</p></td></tr>
<tr><td bgcolor="#eeeeff" align="left" dir="ltr"><pre>
# <span class="code-input">scanelf -l -p -R -q -F "%F #t"</span>
</pre></td></tr>
</table>
<p class="chaphead"><a name="pspax"></a><a name="doc_chap3"></a><span class="chapnum">3.
            </span>Listing PaX Flags and Capabilities</p>
<p class="secthead"><a name="doc_chap3_sect1">About PaX</a></p>
<p>
<a href="http://pax.grsecurity.net">PaX</a> is a project hosted by the <a href="http://www.grsecurity.net">grsecurity</a> project. Quoting the <a href="http://pax.grsecurity.net/docs/pax.txt">PaX documentation</a>, its main 
goal is "to research various defense mechanisms against the exploitation of 
software bugs that give an attacker arbitrary read/write access to the 
attacked task's address space. This class of bugs contains among others 
various forms of buffer overflow bugs (be they stack or heap based), user
supplied format string bugs, etc."
</p>
<p>
To be able to benefit from these defense mechanisms, you need to run a Linux
kernel patched with the latest PaX code. The <a href="http://hardened.gentoo.org">Hardened Gentoo</a> project supports PaX and
its parent project, grsecurity. The supported kernel package is
<span class="code" dir="ltr">sys-kernel/hardened-sources</span>.
</p>
<p>
The Gentoo/Hardened project has a <a href="pax-quickstart.html">Gentoo PaX Quickstart Guide</a>
for your reading pleasure.
</p>
<p class="secthead"><a name="doc_chap3_sect2">Flags and Capabilities</a></p>
<p>
If your toolchain supports it, your binaries can have additional PaX flags in
their Program Header. The following flags are supported:
</p>
<table class="ntable">
<tr>
  <td class="infohead"><b>Flag</b></td>
  <td class="infohead"><b>Name</b></td>
  <td class="infohead"><b>Description</b></td>
</tr>
<tr>
  <td class="tableinfo">P</td>
  <td class="tableinfo">PAGEEXEC</td>
  <td class="tableinfo">
    Refuse code execution on writable pages based on the NX bit
    (or emulated NX bit)
  </td>
</tr>
<tr>
  <td class="tableinfo">S</td>
  <td class="tableinfo">SEGMEXEC</td>
  <td class="tableinfo">
    Refuse code execution on writable pages based on the
    segmentation logic of IA-32
  </td>
</tr>
<tr>
  <td class="tableinfo">E</td>
  <td class="tableinfo">EMUTRAMP</td>
  <td class="tableinfo">
    Allow known code execution sequences on writable pages that
    should not cause any harm
  </td>
</tr>
<tr>
  <td class="tableinfo">M</td>
  <td class="tableinfo">MPROTECT</td>
  <td class="tableinfo">
    Prevent the creation of new executable code to the process
    address space
  </td>
</tr>
<tr>
  <td class="tableinfo">R</td>
  <td class="tableinfo">RANDMMAP</td>
  <td class="tableinfo">
    Randomize the stack base to prevent certain stack overflow
    attacks from being successful
  </td>
</tr>
<tr>
  <td class="tableinfo">X</td>
  <td class="tableinfo">RANDEXEC</td>
  <td class="tableinfo">
    Randomize the address where the application maps to prevent
    certain attacks from being exploitable
  </td>
</tr>
</table>
<p>
The default Linux kernel also supports certain capabilities, grouped in the
so-called <span class="emphasis">POSIX.1e Capabilities</span>. You can find a listing of those
capabilities in our <a href="capabilities.html">POSIX Capabilities</a> document.
</p>
<p class="secthead"><a name="doc_chap3_sect3">Using pspax</a></p>
<p>
The <span class="code" dir="ltr">pspax</span> application, part of the <span class="code" dir="ltr">pax-utils</span> package, displays the
run-time capabilities of all programs you have permission for. On Linux kernels
with additional support for extended attributes (such as SELinux) those
attributes are shown as well.
</p>
<p>
When ran, <span class="code" dir="ltr">pspax</span> shows the following information:
</p>
<table class="ntable">
<tr>
  <td class="infohead"><b>Column</b></td>
  <td class="infohead"><b>Description</b></td>
</tr>
<tr>
  <td class="tableinfo">USER</td>
  <td class="tableinfo">Owner of the process</td>
</tr>
<tr>
  <td class="tableinfo">PID</td>
  <td class="tableinfo">Process id</td>
</tr>
<tr>
  <td class="tableinfo">PAX</td>
  <td class="tableinfo">Run-time PaX flags (if applicable)</td>
</tr>
<tr>
  <td class="tableinfo">MAPS</td>
  <td class="tableinfo">Write/eXecute markings for the process map</td>
</tr>
<tr>
  <td class="tableinfo">ELF_TYPE</td>
  <td class="tableinfo">Process executable type: ET_DYN or ET_EXEC</td>
</tr>
<tr>
  <td class="tableinfo">NAME</td>
  <td class="tableinfo">Name of the process</td>
</tr>
<tr>
  <td class="tableinfo">CAPS</td>
  <td class="tableinfo">POSIX.1e capabilities (see note)</td>
</tr>
<tr>
  <td class="tableinfo">ATTR</td>
  <td class="tableinfo">Extended attributes (if applicable)</td>
</tr>
</table>
<table class="ncontent" width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td bgcolor="#bbffbb"><p class="note"><b>Note: </b>
<span class="code" dir="ltr">pspax</span> only displays these capabilities when it is linked with
the external capabilities library. This requires you to build <span class="code" dir="ltr">pax-utils</span>
with -DWANT_SYSCAP.
</p></td></tr></table>
<p>
By default, <span class="code" dir="ltr">pspax</span> does not show any kernel processes. If you want those
to be taken as well, use the <span class="code" dir="ltr">-a</span> switch.
</p>
<p class="chaphead"><a name="dumpelf"></a><a name="doc_chap4"></a><span class="chapnum">4.
            </span>Programming with ELF files</p>
<p class="secthead"><a name="doc_chap4_sect1">The dumpelf Utility</a></p>
<p>
With the <span class="code" dir="ltr">dumpelf</span> utility you can convert a ELF file into human readable C
code that defines a structure with the same image as the original ELF file.
</p>
<a name="doc_chap4_pre1"></a><table class="ntable" width="100%" cellspacing="0" cellpadding="0" border="0">
<tr><td bgcolor="#7a5ada"><p class="codetitle">Code Listing4.1: dumpelf example</p></td></tr>
<tr><td bgcolor="#eeeeff" align="left" dir="ltr"><pre>
$ <span class="code-input">dumpelf /bin/hostname</span>
#include &lt;elf.h&gt;

<span class="code-comment">/*
 * ELF dump of '/bin/hostname'
 *     10276 (0x2824) bytes
 */</span>

struct {
        Elf32_Ehdr ehdr;
        Elf32_Phdr phdrs[8];
        Elf32_Shdr shdrs[26];
} dumpedelf_0 = {

.ehdr = {
<span class="code-comment">(... Output stripped ...)</span>
</pre></td></tr>
</table>
<br><p class="copyright">
	The contents of this document, unless otherwise expressly stated, are licensed under the <a href="http://creativecommons.org/licenses/by-sa/2.5">CC-BY-SA-2.5</a> license. The <a href="http://www.gentoo.org/main/en/name-logo.xml"> Gentoo Name and Logo Usage Guidelines </a> apply.
  </p>
<!--
  <rdf:RDF xmlns="http://web.resource.org/cc/"
      xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  
  <license rdf:about="http://creativecommons.org/licenses/by-sa/2.5/">
    
     <permits rdf:resource="http://web.resource.org/cc/Reproduction" />
     <permits rdf:resource="http://web.resource.org/cc/Distribution" />
     <requires rdf:resource="http://web.resource.org/cc/Notice" />
     <requires rdf:resource="http://web.resource.org/cc/Attribution" />
     <permits rdf:resource="http://web.resource.org/cc/DerivativeWorks" />
     <requires rdf:resource="http://web.resource.org/cc/ShareAlike" />
  </License>
  </rdf:RDF>
--><br>
</td>
<td width="1%" bgcolor="#dddaec" valign="top"><table border="0" cellspacing="4px" cellpadding="4px">
<tr><td class="topsep" align="center"><p class="altmenu"><a title="View a printer-friendly version" class="altlink" href="swift?style=printable">Print</a></p></td></tr>
<tr><td class="topsep" align="center"><p class="alttext">Page updated August 29, 2010</p></td></tr>
<tr><td class="topsep" align="left"><p class="alttext"><b>Summary: </b>
This guide provides instruction on securing your system by using the pax-utils
package to find and identify problematic binaries.
</p></td></tr>
<tr><td align="left" class="topsep"><p class="alttext">
  <a href="mailto:swift@gentoo.org" class="altlink"><b>Sven Vermeulen</b></a>
<br><i>Author</i><br><br>
  <a href="mailto:solar@gentoo.org" class="altlink"><b>Ned Ludd</b></a>
<br><i>Editor</i><br><br>
  <a href="mailto:nightmorph@gentoo.org" class="altlink"><b>Joshua Saddler</b></a>
<br><i>Editor</i><br></p></td></tr>
<tr lang="en"><td align="center" class="topsep">
<p class="alttext"><b>Donate</b> to support our development efforts.
        </p>
<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_xclick"><input type="hidden" name="business" value="paypal@gentoo.org"><input type="hidden" name="item_name" value="Gentoo Linux Support"><input type="hidden" name="item_number" value="1000"><input type="hidden" name="image_url" value="http://www.gentoo.org/images/paypal.png"><input type="hidden" name="no_shipping" value="1"><input type="hidden" name="return" value="http://www.gentoo.org"><input type="hidden" name="cancel_return" value="http://www.gentoo.org"><input type="image" src="http://images.paypal.com/images/x-click-but21.gif" name="submit" alt="Donate to Gentoo">
</form>
</td></tr>
<tr lang="en"><td align="center"><iframe src="http://sidebar.gentoo.org" scrolling="no" width="125" height="850" frameborder="0" style="border:0px padding:0x" marginwidth="0" marginheight="0"><p>Your browser does not support iframes.</p></iframe></td></tr>
</table></td>
</tr></table></td></tr>
<tr><td colspan="2" align="right" class="infohead">
Copyright 2001-2012 Gentoo Foundation, Inc. Questions, Comments? <a class="highlight" href="http://www.gentoo.org/main/en/contact.xml">Contact us</a>.
</td></tr>
</table></body>
</html>
