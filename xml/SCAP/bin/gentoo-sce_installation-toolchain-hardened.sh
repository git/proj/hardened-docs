#!/bin/sh

# Get CHOST value
echo "Getting CHOST variable content through portageq.";
my_chost=$(portageq envvar CHOST);
if [ -z "${my_chost}" ];
then
  echo "-- The portageq command failed. Falling back to glibc build info.";
  my_chost=$(cat /var/db/pkg/sys-libs/glibc-*/CHOST | tail -1);
fi
echo "-- Got CHOST=${my_chost}";

# Get current GCC version
echo "Getting current GCC version through /etc/env.d/gcc/config-*";
current_gcc=$(grep CURRENT /etc/env.d/gcc/config-* | sed -e "s:CURRENT=${my_chost}-::g" | sed -e "s:\([0-9\.-r]*\){$,-.*$}:\1:g" );
echo "-- Got version=${current_gcc}";

# Get type
echo "Getting compiler type (profile/spec) through its CURRENT= value.";
current_type=$(grep CURRENT /etc/env.d/gcc/config-* | sed -e "s:CURRENT=${my_chost}-${current_gcc}::g" | sed -e 's:^-::g');
echo "-- Got type=${current_type}";

echo "Checking USE flags of gcc-${current_gcc} for hardened USE flag.";
grep -q hardened /var/db/pkg/sys-devel/gcc-${current_gcc}/USE;
current_hardened_use=$?;

if [ ${current_hardened_use} -ne 0 ];
then
  echo "!! GCC ${current_gcc} is not build with USE=hardened!";
  echo "!! Please enable a hardened profile.";
  exit ${XCCDF_RESULT_FAIL};
else
  echo "-- GCC ${current_gcc} is build with USE=hardened.";
  if [ -z "${current_type}" ];
  then
    echo "-- The default type is used which is a hardened type.";
    exit ${XCCDF_RESULT_PASS};
  else
    echo "!! A non-default type is used: ${current_type}";
    echo "!! This means not all hardened toolchain measures are enabled.";
    exit ${XCCDF_RESULT_FAIL};
  fi
fi
