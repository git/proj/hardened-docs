#!/bin/sh

if [ $# -ne 1 ];
then
  echo "Usage: $0 --justfixit";
  echo "";
  echo "This script will update the (generated) HTML files inside this directory"
  echo "by correcting the locations of the CSS file and images. This allows for"
  echo "the gitweb interface to display the HTMLs (when selected 'raw') correctly."
  exit 1;
fi

for FILE in `find html/ -iname '*.html'`;
do
  sed -i -e 's|"/css/main.css"|"http://www.gentoo.org/css/main.css"|g' ${FILE};
  sed -i -e 's|"../../../|"http://www.gentoo.org/|g' ${FILE};
  sed -i -e 's|"/images/|"http://www.gentoo.org/images/|g' ${FILE};
  sed -i -e 's|"/|"http://www.gentoo.org/|g' ${FILE};
  cp ${FILE} ${FILE}.orig;
  cat ${FILE}.orig | tr -d "\302" | tr -d "\240" > ${FILE};
  rm ${FILE}.orig;
done
